$(document).ready(function() {
  $('.border').each(function() {
    var _this = $(this);
    var borderWidth = _this.css("border-width");
    var parent = _this.parent();
    
    borderWidth = Number(borderWidth.replace(/[^0-9\.]+/g,""));
    _this.width(parent.width() - borderWidth * 2);
    _this.height(parent.height() - borderWidth * 2);
  });
});  